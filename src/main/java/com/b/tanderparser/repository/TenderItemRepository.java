package com.b.tanderparser.repository;

 import com.b.tanderparser.model.TenderItem;
 import org.springframework.data.jpa.repository.Query;
 import org.springframework.data.repository.query.Param;
 import org.springframework.stereotype.Repository;


 import java.util.List;

@Repository
public interface TenderItemRepository extends GenericRepository<TenderItem> {

 @Query(value = "SELECT DISTINCT ti.tender_id FROM tender_items ti " +
                "WHERE EXISTS (SELECT 1 FROM tender_items ti2 WHERE ti2.tender_id = ti.tender_id AND ti2.name ILIKE :searchTerm) " +
                "AND NOT EXISTS (SELECT 1 FROM tender_items ti3 WHERE ti3.tender_id = ti.tender_id AND ti3.name NOT ILIKE :searchTerm)",
         nativeQuery = true)
 List<Long> findByDescriptionContainingIgnoreCase(@Param("searchTerm") String searchTerm);





}
