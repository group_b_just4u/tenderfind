package com.b.tanderparser.repository;

import com.b.tanderparser.dto.tender.TenderIdDTO;
import com.b.tanderparser.model.TenderItem;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import java.util.Arrays;
import java.util.List;


@Repository
public class QueryRepository {

    private final JdbcTemplate jdbcTemplate;

    public QueryRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    public List<Long> findByWordsInName(String[] words) {

        String[] likeWords = Arrays.stream(words)
                .map(word -> "%" + word + "%")
                .toArray(String[]::new);

        String query = "SELECT tender_id " +
                "FROM tender_items " +
                "GROUP BY tender_id " +
                "HAVING not bool_or(name NOT ILIKE ALL(?))";

        Object[] params = {likeWords}; // Передаем массив строк прямо в параметры

        List<Long> result = jdbcTemplate.queryForList(query, Long.class, params);
        return result;
    }

    public List<TenderItem> findByIdInTenderId(Long tenderId) {
        String query = "SELECT * FROM tender_items " +
                "where tender_id = "+ tenderId;
        System.out.println(query);
        List<TenderItem> lti= jdbcTemplate.query(query, new BeanPropertyRowMapper<>(TenderItem.class));
        System.out.println(lti);
        return lti;

    }


//    public List<TenderIdDTO> findAllId() {
//        String query = "SELECT id, company_id  FROM tenders";
//        return jdbcTemplate.queryForList(query, TenderIdDTO.class);
//    }

    public List<TenderIdDTO> findAllId() {
        String query = "SELECT id, company_id FROM tenders"; // Выберите все необходимые поля
        return jdbcTemplate.query(query, (rs, rowNum) -> {
            TenderIdDTO tenderIdDTO = new TenderIdDTO();
            tenderIdDTO.setId(rs.getLong("id"));
            tenderIdDTO.setCompanyId(rs.getLong("company_id"));
            return tenderIdDTO;
        });
    }


}
