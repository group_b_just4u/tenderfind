package com.b.tanderparser.repository;

import com.b.tanderparser.model.Tender;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TenderRepository extends GenericRepository<Tender> {

    @Query("""
            SELECT MAX(id) FROM Tender
                        """)
    Long maxId();

    List<Tender> findByIdIn(List<Long> ids);

}