package com.b.tanderparser.mapper;

 import com.b.tanderparser.dto.tenderlist.DataItem;
 import com.b.tanderparser.model.Tender;
 import org.modelmapper.ModelMapper;
 import org.springframework.stereotype.Component;

@Component
public class TenderMapper {

    private final ModelMapper modelMapper;

    public TenderMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public Tender mapTenderToEntity(DataItem dataItem) {
        return modelMapper.map(dataItem, Tender.class);
    }

}

