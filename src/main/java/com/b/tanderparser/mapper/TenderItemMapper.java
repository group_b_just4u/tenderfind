package com.b.tanderparser.mapper;

import com.b.tanderparser.dto.tender.TenderItemDTO;
import com.b.tanderparser.model.TenderItem;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class TenderItemMapper {
    private final ModelMapper modelMapper;

    public TenderItemMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public TenderItem mapTenderItemDTOToEntity(TenderItemDTO tenderItemDTO) {
        System.out.println("mapTenderItemDTOToEntity");
        return modelMapper.map(tenderItemDTO, TenderItem.class);
    }

}
