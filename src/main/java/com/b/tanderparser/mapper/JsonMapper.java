package com.b.tanderparser.mapper;

import com.b.tanderparser.dto.tender.TenderItemDTO;
import com.b.tanderparser.dto.tenderlist.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;
import java.util.List;

@Service
public class JsonMapper {

    private final ObjectMapper objectMapper;

    @Autowired
    public JsonMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public Data processJson(String json) {
        try {
            Data data = objectMapper.readValue(json, Data.class);
            return data;
        } catch (Exception e) {
            e.printStackTrace();
            // Обработка ошибок
        }
        throw new NullPointerException("result must be not null");
    }

    public String convertToJson(Data data) {
        try {
            return objectMapper.writeValueAsString(data);
        } catch (Exception e) {
            e.printStackTrace();
            // Обработка ошибок
            return null;
        }
    }



    public List<TenderItemDTO> processDataField(String json) {
        try {
            List<TenderItemDTO> tenderItemDTOList = new ArrayList<>();
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode rootNode = objectMapper.readTree(json);

            JsonNode dataNode = rootNode.get("result").get("data");
            if (dataNode.isArray()) {
                for (JsonNode itemNode : dataNode) {
                    System.out.println(itemNode);
                    TenderItemDTO tenderItemDTO = objectMapper.treeToValue(itemNode, TenderItemDTO.class);
                    tenderItemDTOList.add(tenderItemDTO);
                }
            }
            return tenderItemDTOList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new NullPointerException("result must be not null");
    }
}
