package com.b.tanderparser.service;


import com.b.tanderparser.dto.tender.TenderIdDTO;
import com.b.tanderparser.mapper.TenderMapper;
import com.b.tanderparser.model.Tender;
import com.b.tanderparser.repository.QueryRepository;
import com.b.tanderparser.repository.TenderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class TenderService {

    private final TenderRepository tenderRepository;
    private final TenderMapper tenderMapper;
    private final QueryRepository queryRepository;

    @Autowired
    public TenderService(TenderRepository tenderRepository, TenderMapper tenderMapper, QueryRepository queryRepository) {
        this.tenderRepository = tenderRepository;
        this.tenderMapper = tenderMapper;
        this.queryRepository = queryRepository;
    }



    public void saveTender(Tender tender) {
        tenderRepository.save(tender);
    }

    public void saveTenders(List<Tender> tenders) {
         tenderRepository.saveAll(tenders);
    }

    public List<Tender> getTendersByIdIn(List listId){
        return tenderRepository.findByIdIn(listId);
    }


    public Long getLastId() {
        return tenderRepository.maxId();
    }

    public List<TenderIdDTO> getAllId() {
        return queryRepository.findAllId();
    }



}
