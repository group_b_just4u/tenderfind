package com.b.tanderparser.service;

import com.b.tanderparser.mapper.TenderItemMapper;
import com.b.tanderparser.model.TenderItem;
import com.b.tanderparser.repository.QueryRepository;
import com.b.tanderparser.repository.TenderItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TenderItemService {


    private final TenderItemRepository tenderItemRepository;
    private final QueryRepository queryRepository;

    @Autowired
    public TenderItemService(TenderItemRepository tenderItemRepository,            QueryRepository queryRepository) {
        this.tenderItemRepository = tenderItemRepository;
        this.queryRepository = queryRepository;
    }

    @Transactional
    public void saveTenderItems(List<TenderItem> tenderItems) {
        tenderItemRepository.saveAll(tenderItems);
    }

    public List<Long> findTendersByDescription(String searchTerm) {
        return tenderItemRepository.findByDescriptionContainingIgnoreCase(searchTerm);
    }


    public List<Long> findEntitiesByWords(String[] words) {
        return queryRepository.findByWordsInName(words);
    }

    public List<TenderItem> getOne(Long id) {
        try {
            return queryRepository.findByIdInTenderId(id);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public void clearTable() {
        tenderItemRepository.deleteAll();
    }
}
