package com.b.tanderparser.dto.tender;

import lombok.*;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TenderIdDTO {
    long id;
    long companyId;
}
