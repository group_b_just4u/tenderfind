package com.b.tanderparser.dto.tender;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class TenderItemDTO {
    @JsonProperty("vat")
    private String vat;
    @JsonProperty("number")
    private Long number;
    @JsonProperty("analog_id")
    private Long analogId;
    @JsonProperty("gost")
    private String gost;
    @JsonProperty("start_price_with_vat")
    private String startPriceWithVat;
    @JsonProperty("tender_id")
    private Long tenderId;
    @JsonProperty("winnerid")
    private String winnerId;
    @JsonProperty("id")
    private Long id;
    @JsonProperty("unit_name")
    private String unitName;
    @JsonProperty("anno")
    private String anno;
    @JsonProperty("analog_name")
    private String analogName;
    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("start_price")
    private String startPrice;
    @JsonProperty("max_price")
    private String maxPrice;
    @JsonProperty("id_client")
    private String idClient;
    @JsonProperty("amount")
    private String amount;
    @JsonProperty("n_id")
    private Long nId;
    @JsonProperty("custom_field")
    private String customField;
    @JsonProperty("max_price_with_vat")
    private String maxPriceWithVat;

}
