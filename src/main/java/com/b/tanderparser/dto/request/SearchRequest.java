package com.b.tanderparser.dto.request;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class SearchRequest {
    private boolean checkbox;
    private String keywords;

 }

