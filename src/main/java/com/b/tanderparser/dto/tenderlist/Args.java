package com.b.tanderparser.dto.tenderlist;

import com.fasterxml.jackson.annotation.JsonProperty;



public class Args {
    @JsonProperty("types")
    private String[] types;
    @JsonProperty("open_only")
    private String openOnly;
    @JsonProperty("set_type_id")
    private String setTypeId;
    @JsonProperty("_key")
    private String key;
    @JsonProperty("set_id")
    private String setId;
    @JsonProperty("max_rows")
    private String maxRows;
    @JsonProperty("offset")
    private String offset;


}


