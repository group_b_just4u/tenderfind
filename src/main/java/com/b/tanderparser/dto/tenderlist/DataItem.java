package com.b.tanderparser.dto.tenderlist;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DataItem {
    @JsonProperty("company_rating")
    private String companyRating;
    @JsonProperty("is_delivery_required")
    private int isDeliveryRequired;
    @JsonProperty("company_name")
    private String companyName;
    @JsonProperty("delivery_area_id")
    private int deliveryAreaId;
    @JsonProperty("close_date")
    private String closeDate;
    @JsonProperty("ship_date")
    private String shipDate;
    @JsonProperty("open_date")
    private String openDate;
    @JsonProperty("type_id")
    private int typeId;
    @JsonProperty("id")
    private int id;
    @JsonProperty("own_currensies")
    private Object ownCurrensies;
    @JsonProperty("status_id")
    private int statusId;
    @JsonProperty("type_name")
    private String typeName;
    @JsonProperty("company_id")
    private int companyId;
    @JsonProperty("delivery_address")
    private String deliveryAddress;
    @JsonProperty("dinamic_fields")
    private Object dinamicFields;
    @JsonProperty("currency_id")
    private int currencyId;
    @JsonProperty("is_priv")
    private int isPriv;
    @JsonProperty("is_hidden")
    private int isHidden;
    @JsonProperty("delivery_country_id")
    private int deliveryCountryId;
    @JsonProperty("delivery_region_id")
    private int deliveryRegionId;
    @JsonProperty("currency_name")
    private String currencyName;
    @JsonProperty("possibleclose_date")
    private Object possibleCloseDate;
    @JsonProperty("finish_date")
    private Object finishDate;
    @JsonProperty("date_time_sogl")
    private String dateTimeSogl;
    @JsonProperty("title")
    private String title;

    @Override
    public String toString() {
        return "DataItem{" +
                "companyRating='" + companyRating + '\'' +
                ", isDeliveryRequired=" + isDeliveryRequired +
                ", companyName='" + companyName + '\'' +
                ", deliveryAreaId=" + deliveryAreaId +
                ", closeDate='" + closeDate + '\'' +
                ", shipDate='" + shipDate + '\'' +
                ", openDate='" + openDate + '\'' +
                ", typeId=" + typeId +
                ", id=" + id +
                ", ownCurrensies=" + ownCurrensies +
                ", statusId=" + statusId +
                ", typeName='" + typeName + '\'' +
                ", companyId=" + companyId +
                ", deliveryAddress='" + deliveryAddress + '\'' +
                ", dinamicFields=" + dinamicFields +
                ", currencyId=" + currencyId +
                ", isPriv=" + isPriv +
                ", isHidden=" + isHidden +
                ", deliveryCountryId=" + deliveryCountryId +
                ", deliveryRegionId=" + deliveryRegionId +
                ", currencyName='" + currencyName + '\'' +
                ", possibleCloseDate=" + possibleCloseDate +
                ", finishDate=" + finishDate +
                ", dateTimeSogl='" + dateTimeSogl + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
