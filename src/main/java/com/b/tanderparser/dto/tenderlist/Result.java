package com.b.tanderparser.dto.tenderlist;

import lombok.Getter;

import java.util.List;

@Getter
public class Result {
    private Args args;
    private List<DataItem> data;
 }
