package com.b.tanderparser.model;

import jakarta.persistence.*;
import lombok.*;


@Entity
@Table(name = "tenders")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Tender extends GenericModel {

        private String companyRating;
        private int isDeliveryRequired;
        private String companyName;
        private int deliveryAreaId;
        private String closeDate;
        private String shipDate;
        private String openDate;
        private int typeId;
        private int companyId;
        private String deliveryAddress;
        private int currencyId;
        private int isPriv;
        private int isHidden;
        private int deliveryCountryId;
        private int deliveryRegionId;
        private String currencyName;
        private String dateTimeSogl;
        @Column(length = -1)
        private String title;

        @Override
        public String toString() {
                return "Tender{" +
                        "companyRating='" + companyRating + '\'' +
                        ", isDeliveryRequired=" + isDeliveryRequired +
                        ", companyName='" + companyName + '\'' +
                        ", deliveryAreaId=" + deliveryAreaId +
                        ", closeDate='" + closeDate + '\'' +
                        ", shipDate='" + shipDate + '\'' +
                        ", openDate='" + openDate + '\'' +
                        ", typeId=" + typeId +
                        ", companyId=" + companyId +
                        ", deliveryAddress='" + deliveryAddress + '\'' +
                        ", currencyId=" + currencyId +
                        ", isPriv=" + isPriv +
                        ", isHidden=" + isHidden +
                        ", deliveryCountryId=" + deliveryCountryId +
                        ", deliveryRegionId=" + deliveryRegionId +
                        ", currencyName='" + currencyName + '\'' +
                        ", dateTimeSogl='" + dateTimeSogl + '\'' +
                        ", title='" + title + '\'' +
                        "} " + super.toString();
        }
}















