package com.b.tanderparser.model;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.*;

@Entity
@Table(name = "tender_items")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TenderItem extends GenericModel {
    private String vat;
    private Long number;
    private Long analogId;
    private String gost;
    private String startPriceWithVat;
    private Long tenderId;
    private String winnerId;
    private String unitName;
    @Column(length = -1)
    private String anno;
    private String analogName;
    @Column(length = -1)
    private String name;
    @Column(length = -1)
    private String description;
    private String startPrice;
    private String maxPrice;
    private String idClient;
    private String amount;
    private Long nId;
    private String customField;
    private String maxPriceWithVat;
}
