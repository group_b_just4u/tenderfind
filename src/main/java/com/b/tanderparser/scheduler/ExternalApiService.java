package com.b.tanderparser.scheduler;


import com.b.tanderparser.dto.tender.TenderIdDTO;
import com.b.tanderparser.dto.tender.TenderItemDTO;
import com.b.tanderparser.dto.tenderlist.DataItem;
import com.b.tanderparser.mapper.JsonMapper;
import com.b.tanderparser.mapper.TenderItemMapper;
import com.b.tanderparser.mapper.TenderMapper;
import com.b.tanderparser.model.Tender;
import com.b.tanderparser.model.TenderItem;
import com.b.tanderparser.service.TenderItemService;
import com.b.tanderparser.service.TenderService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.client.RestTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ExternalApiService {

    private final RestTemplate restTemplate;
    private final JsonMapper jsonMapper;

    private final TenderMapper tenderMapper;
    private final TenderService tenderService;

    private TenderItemMapper tenderItemMapper;
    private TenderItemService tenderItemService;


    public ExternalApiService(RestTemplate restTemplate, JsonMapper jsonMapper, TenderMapper tenderMapper, TenderService tenderService, TenderItemMapper tenderItemMapper, TenderItemService tenderItemService) {
        this.restTemplate = restTemplate;
        this.jsonMapper = jsonMapper;
        this.tenderMapper = tenderMapper;
        this.tenderService = tenderService;
        this.tenderItemMapper = tenderItemMapper;
        this.tenderItemService = tenderItemService;
    }

    @Async
    @Scheduled(fixedRate = 60000) // Запускать каждую минуту/2
    public void fetchDataFromExternalApi() {
        Long time = System.nanoTime() / 1000000;
        System.out.println(time);

        String urlTenderlist = "https://www.tender.pro/api/_info.tenderlist_by_set.json" +
                "?_key=1732ede4de680a0c93d81f01d7bac7d1" +
                "&set_type_id=2" +
                "&set_id=875000" +
                "&max_rows=100" +
                "&open_only=t";
        String response = restTemplate.getForObject(urlTenderlist, String.class);
        List<DataItem> listDI = jsonMapper.processJson(response).getResult().getData();

        Long lastId = tenderService.getLastId();
        long finalLastId = (lastId == null) ? 1L : lastId;
        List<Tender> tenders = listDI.stream()
                .filter(item -> item.getId() > finalLastId)
                .map(tenderMapper::mapTenderToEntity)
                .toList();


        if (!tenders.isEmpty()) for (Tender i : tenders) tenderService.saveTender(i);


        ///////////////////////////////

        List<TenderIdDTO> tendersIds= extractIdsFromTenders(tenders);
        sendRequestItemAndSave(tendersIds);


        System.out.println(System.nanoTime() / 1000000 -time);


    }

    private void sendRequestItemAndSave(List<TenderIdDTO> tenderIdDTO) {

        String urlTenderItem = "https://www.tender.pro/api/_tender.item.json";
        String key = "?_key=1732ede4de680a0c93d81f01d7bac7d1";
        String company = "&company_id=";
        String tenderId = "&id=";

        for (TenderIdDTO i : tenderIdDTO) {

            String uri = urlTenderItem
                    + key
                    + company
                    + i.getCompanyId()
                    + tenderId
                    + i.getId();

            String response = restTemplate.getForObject(uri, String.class);
            if (response == null) continue;
            if (response.length() < 300) continue;

            List<TenderItemDTO> tenderItemDTOList =
                    jsonMapper.processDataField(response);


            List<TenderItem> tenderItems = new ArrayList<>();
            for (TenderItemDTO tenderItemDTO : tenderItemDTOList) {
                TenderItem tenderItem = tenderItemMapper.mapTenderItemDTOToEntity(tenderItemDTO);
                tenderItems.add(tenderItem);
            }
            tenderItemService.saveTenderItems(tenderItems);

            System.out.println(System.nanoTime() / 1000000);

            ///////////////////////////////
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public List<TenderIdDTO> extractIdsFromTenders(List<Tender> tenders) {

        return tenders.stream()
                .map(x->new TenderIdDTO(x.getId(),x.getCompanyId()))
                .toList();
    }

    @Async
    @Scheduled(fixedRate = 6000000) // Запускать каждую минуту/2
    public void updateDataFromExternalApi() {
        List<TenderIdDTO> ids = tenderService.getAllId();
        System.out.println(ids);
        tenderItemService.clearTable();
        sendRequestItemAndSave(ids);
        System.out.println("finish");

    }


}
