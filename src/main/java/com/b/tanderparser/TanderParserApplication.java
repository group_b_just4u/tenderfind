package com.b.tanderparser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class TanderParserApplication {

    public static void main(String[] args) {
        SpringApplication.run(TanderParserApplication.class, args);
    }

}
