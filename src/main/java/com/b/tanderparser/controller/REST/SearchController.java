package com.b.tanderparser.controller.REST;

import com.b.tanderparser.dto.request.SearchRequest;
import com.b.tanderparser.model.Tender;
import com.b.tanderparser.service.TenderItemService;
import com.b.tanderparser.service.TenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class SearchController {

    private final TenderService tenderService;
    private final TenderItemService tenderItemService;

    @Autowired
    public SearchController(TenderService tenderService, TenderItemService tenderItemService) {
        this.tenderService = tenderService;
        this.tenderItemService = tenderItemService;
    }

    @PostMapping("/search")
    public List<Tender> search(@RequestBody SearchRequest request) {

        System.out.println(request);

        String[] array =  request.getKeywords().split(",");
        for (int i = 0; i < array.length; i++) {
            array[i]=  array[i].replaceAll(" ","%");
        }

        System.out.println(Arrays.toString(array));

        //List<Long> results = tenderItemService.findTendersByDescription("%"+request.getKeywords()+"%");

        List<Long> results = tenderItemService.findEntitiesByWords(array);
        System.out.println(results);

        return tenderService.getTendersByIdIn(results);
    }
}