package com.b.tanderparser.controller.REST;

import com.b.tanderparser.model.TenderItem;
import com.b.tanderparser.service.TenderItemService;
import com.b.tanderparser.service.TenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class TenderItemController {

    private final TenderService tenderService;

    private final TenderItemService tenderItemService;

    @Autowired
    public TenderItemController(TenderService tenderService, TenderItemService tenderItemService) {
        this.tenderService = tenderService;
        this.tenderItemService = tenderItemService;
    }

    @GetMapping("/getTenderItem")
    public List<TenderItem> search(@RequestParam("TenderId") Long tenderId) {
        System.out.println("получен TenderId ="+tenderId);
        return tenderItemService.getOne(tenderId);
    }
}