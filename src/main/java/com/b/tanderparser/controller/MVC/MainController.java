package com.b.tanderparser.controller.MVC;

 import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class MainController {


    @GetMapping("/")
    public ModelAndView index() {

        return new ModelAndView("homepage.html");
}

    @GetMapping("/find")
    public ModelAndView find() {

        return new ModelAndView("index.html");
    }

    @GetMapping("/tenderItem")
    public ModelAndView Item() {

        return new ModelAndView("item.html");
    }

}

