Реализованная функциональность
Получение информации о тендерах;
Поиск среди полученной информации;
Рекомендации по тендерам;
Основной стек технологий:
Java, SpringFramework,
HTML, CSS, JavaScript,
PostgreSQL,
Maven, Gitlab.

СРЕДА ЗАПУСКА
IDE IntelliJ IDEA
Doker:
tanderParser\docker-compose-app.yml
tanderParser\docker-compose-db.yml

УСТАНОВКА
IntelliJ IDEA :
java 17  boot class: com.b.tanderparser.TanderParserApplication
или 
tanderParser\docker-compose-app.yml
...
База данных
запустить файл
tanderParser\docker-compose-db.yml
через IDE IntelliJ IDEA
запустить проект и 
БД будет заполнена через минуту

 
РАЗРАБОТЧИК
Гафаров РС @GRS801